package gojek.alldemo.com.gojek_test

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import gojek.alldemo.com.gojek_test.MyApplication.Companion.context
import gojek.alldemo.com.gojek_test.R.id.*
import gojek.alldemo.com.gojek_test.datamodel.CustomAdapter
import gojek.alldemo.com.gojek_test.datamodel.DataResponse
import gojek.alldemo.com.gojek_test.network_hit.RetrofitFactory
import okhttp3.Cache
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : BaseActivity(), SwipeRefreshLayout.OnRefreshListener {
    override fun onRefresh() {
       // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        swipeContainer.isRefreshing=true;
        startFetchingRecord();
    }


    companion object {

        val REQUEST_ID_MULTIPLE_PERMISSIONS = 1
        val cacheSize = (5 * 1024 * 1024).toLong()
        val myCache = Cache(context!!.cacheDir, cacheSize)
        private val SPLASH_TIME_OUT = 2000
    }

    val list=ArrayList<DataResponse>();


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main);

        findViewById<RelativeLayout>(R.id.no_data).visibility= View.GONE
        findViewById<RelativeLayout>(R.id.data).visibility= View.VISIBLE


        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        plant_list.layoutManager=llm;

        swipeContainer.setOnRefreshListener(this);
        swipeContainer.setEnabled(true);


        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        getCurrentData();




    }


    override fun onPause() {
        super.onPause()

        shimmer_view_container!!.stopShimmerAnimation();
    }


    internal fun getCurrentData() {



        if (checkAndRequestPermissions()) {
            // carry on the normal flow, as the case of  permissions  granted.
            startFetchingRecord();
        }



    }


    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_ID_MULTIPLE_PERMISSIONS -> {

                val perms = HashMap<String, Int>()
                // Initialize the map with both permissions
                perms[Manifest.permission.ACCESS_NETWORK_STATE] = PackageManager.PERMISSION_GRANTED
                // Fill with actual results from user
                if (grantResults.size > 0) {
                    for (i in permissions.indices)
                        perms[permissions[i]] = grantResults[i]
                    // Check for both permissions
                    if (perms[Manifest.permission.ACCESS_NETWORK_STATE] == PackageManager.PERMISSION_GRANTED) {

                        startFetchingRecord();

                    }
                    else
                    {
                        startFetchingRecord();
                    }
                }
            }
        }

    }




    fun startFetchingRecord()
    {

        shimmer_view_container.visibility=View.VISIBLE;
        shimmer_view_container.startShimmerAnimation();




        val retrofit = RetrofitFactory.makeRetrofitService(this);
        val service = retrofit.getListOfRepo();

        service.enqueue(object : Callback<ArrayList<DataResponse>> {
            override fun onResponse(call: Call<ArrayList<DataResponse>>, response: Response<ArrayList<DataResponse>>) {



                runOnUiThread { kotlin.run {

                    shimmer_view_container.stopShimmerAnimation()
                    shimmer_view_container.visibility=View.GONE;

                    if(swipeContainer.isRefreshing) {
                        swipeContainer.isRefreshing = false;
                        list.clear();

                        plant_list.adapter!!.notifyDataSetChanged();

                    }

                    if (response.code() == 200) {
                        val weatherResponse = response.body()!!
                        if(weatherResponse!!.size>0)
                        {
                            list.addAll(weatherResponse);
                            val adapter = CustomAdapter(list)
                            plant_list.adapter = adapter
                        }

                    }

                } }



            }

            override fun onFailure(call: Call<ArrayList<DataResponse>>, t: Throwable) {

                runOnUiThread {
                    kotlin.run {

                        shimmer_view_container.stopShimmerAnimation()
                        shimmer_view_container.visibility=View.GONE;
                        if (swipeContainer.isRefreshing)
                            swipeContainer.isRefreshing = false;

                        Log.e("abhi", "test")
                        findViewById<RelativeLayout>(R.id.data).visibility = View.GONE
                        findViewById<RelativeLayout>(R.id.no_data).visibility = View.VISIBLE
                    }
                }
            }
        })
    }


    private fun checkAndRequestPermissions(): Boolean {

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            val permissionNetworkState = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE)

            val listPermissionsNeeded = ArrayList<String>()
            if (permissionNetworkState != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_NETWORK_STATE)
            }
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toTypedArray(), REQUEST_ID_MULTIPLE_PERMISSIONS)
                return false
            }
        }
        return true
    }









}
