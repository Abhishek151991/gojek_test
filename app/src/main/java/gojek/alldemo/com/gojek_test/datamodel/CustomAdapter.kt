package gojek.alldemo.com.gojek_test.datamodel

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ramotion.foldingcell.FoldingCell
import gojek.alldemo.com.gojek_test.R

class CustomAdapter (val userList: ArrayList<DataResponse>) : RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_layout, parent, false)
        return ViewHolder(v)
    }




    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CustomAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList[position],position)


    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var unfoldedIndexes = HashSet<Int>()

        fun bindItems(user: DataResponse,position:Int) {
            val textViewName = itemView.findViewById(R.id.textViewUsername) as TextView
            val textViewAddress  = itemView.findViewById(R.id.textViewAddress) as TextView
            val folder_view=itemView.findViewById(R.id.folder_view) as FoldingCell
            textViewName.text = user.name
            textViewAddress.text = user.full_name

            // for existing cell set valid valid state(without animation)
            if (unfoldedIndexes.contains(position)) {
                folder_view.unfold(true);
            } else {
                folder_view.fold(true);
            }

            folder_view.setOnClickListener(View.OnClickListener {

                folder_view.toggle(false)

                this.registerToggle(position)

            })


        }


        fun registerFold(position: Int) {
            unfoldedIndexes.remove(position)
        }

        fun registerUnfold(position: Int) {
            unfoldedIndexes.add(position)
        }


        // simple methods for register cell state changes
        fun registerToggle(position: Int) {
            if (unfoldedIndexes.contains(position))
                registerFold(position)
            else
                registerUnfold(position)
        }

    }



    open fun clear()
    {
        userList.clear();
        notifyDataSetChanged();
    }


    open fun addAll(userListData: List<DataResponse>)
    {
        userList.addAll(userListData);
        notifyDataSetChanged();
    }


}