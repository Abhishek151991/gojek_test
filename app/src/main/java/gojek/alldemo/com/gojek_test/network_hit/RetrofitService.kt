package gojek.alldemo.com.gojek_test.network_hit


import gojek.alldemo.com.gojek_test.datamodel.DataResponse
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET

interface RetrofitService {


    @GET("repositories?since=300")
    fun getListOfRepo(): Call<ArrayList<DataResponse>>

}