package gojek.alldemo.com.gojek_test.network_hit

import com.google.gson.Gson
import gojek.alldemo.com.gojek_test.MainActivity
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitFactory {

    const val BASE_URL = "https://api.github.com/"



    fun makeRetrofitService(activity:MainActivity): RetrofitService {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getOkHttpRespose(activity))
                .build().create(RetrofitService::class.java)
    }


    //https://medium.com/mindorks/caching-with-retrofit-store-responses-offline-71439ed32fda
    fun getOkHttpRespose(activity:MainActivity):OkHttpClient{

        val okHttpClient = OkHttpClient.Builder()
                // Specify the cache we created earlier.
                .cache(MainActivity.myCache)
                // Add an Interceptor to the OkHttpClient.
                .addInterceptor { chain ->

                    // Get the request from the chain.
                    var request = chain.request()

                    /*
                    *  Leveraging the advantage of using Kotlin,
                    *  we initialize the request and change its header depending on whether
                    *  the device is connected to Internet or not.
                    */
                    request = if (activity.hasNetwork(activity)!!)
                    /*
                    *  If there is Internet, get the cache that was stored 2 Hour ago.
                    *  If the cache is older than 2 Hour, then discard it,
                    *  and indicate an error in fetching the response.
                    *  The 'max-age' attribute is responsible for this behavior.
                    */
                        request.newBuilder().header("Cache-Control", "public, max-age=" + 60 * 60 * 2).build()
                    else
                    /*
                    *  If there is no Internet, get the cache that was stored 2 Hour ago.
                    *  If the cache is older than 2 Hour, then discard it,
                    *  and indicate an error in fetching the response.
                    *  The 'max-stale' attribute is responsible for this behavior.
                    *  The 'only-if-cached' attribute indicates to not retrieve new data; fetch the cache only instead.
                    */
                        request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 2).build()
                    // End of if-else statement

                    // Add the modified request to the chain.
                    chain.proceed(request)
                }
                .build()

        return okHttpClient;
    }


}