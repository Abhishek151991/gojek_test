package gojek.alldemo.com.gojek_test

import android.app.Application
import android.content.Context

class MyApplication:Application() {

    //Create static ref
    companion object {
        var context: Context? = null
    }

    override fun onCreate() {
        super.onCreate()
        MyApplication.context = getApplicationContext();
    }
}